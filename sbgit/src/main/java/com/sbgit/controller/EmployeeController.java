package com.sbgit.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sbgit.model.User;

@RestController
public class EmployeeController {

	@GetMapping(value="/emp")
	public User getEmployee()
	{
		return new User(10,"Hello","World");
	}
	
	@GetMapping(value="/search/{id}")
	public User getUserById(@PathVariable("id") int userId) {
		User user = null;
		if(userId == 10)
		{
			user = new User(10,"Hello","world");
		}
		if(userId == 20)
		{
			user = new User(20,"Hello","India");
		}
		return user;

		
	}
	
	public List<User> getUsers()
	{
		User user1 = new User(10,"Hello","World");
		User user2 = new User(20,"Good","Evening");
		List<User> users = new ArrayList<>();
		users.add(user1);
		users.add(user2);
		return users;
		
		
	}
}
